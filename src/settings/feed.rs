use serde::{Deserialize, Serialize};

#[derive(Default, Clone, Debug, Serialize, Deserialize)]
pub struct FeedSettings {
    pub use_mathjax: bool,
    pub inline_math: Option<String>,
}
